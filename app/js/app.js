angular.module('testApp', []).

controller('tableController', ($scope) => {
    $scope.organizationList = [
        {
            name: "ДП «ІРЦ»",
            id: "33499803",
            registrationDate: "17-08-1994",
            registrationAddress: "Kyiv, Kybalchycha str. 25"
        },
        {
            name: 'Blitzkrieg',
            id: "1234",
            registrationDate: "22-06-1941",
            registrationAddress: "Berlin, ArbeitMachtFrei str. 1"
        },
        {
            name: "ООО \"Рога и копыта\"",
            id: "6620654",
            registrationDate: "13-10-2017",
            registrationAddress: "Винница, Келецкая 98"
        },
        {
            "name":"One more item",
            "id":"111222333",
            "registrationDate":"2017-07-12T21:00:00.000Z",
            "registrationAddress":"Defaul city, Some str. 48"
        }
    ];

    if (!window.localStorage["organizations"]) {
        console.log("localstorage is empty");
        window.localStorage["organizations"] = JSON.stringify($scope.organizationList);
        console.log("setting up localstorage basic data...");

        if(window.localStorage["organizations"]) {
            console.log("success");
        } else {
            console.log("something went wrong...");
        }
    }

    $scope.organizationsStorage = JSON.parse(window.localStorage["organizations"]);
    $scope.selectedItem = $scope.organizationsStorage[0];

    $scope.changeOrganizationData = () => {
        for (key in $scope.organizationsStorage) {
            if ($scope.organizationsStorage[key].id == $scope.selectedItem.id) {
                $scope.organizationsStorage[key].name == $scope.selectedItem.name;
                $scope.organizationsStorage[key].registrationDate == $scope.selectedItem.registrationDate;
                $scope.organizationsStorage[key].registrationAddress == $scope.selectedItem.registrationAddress;
            }
        }

        window.localStorage["organizations"] = angular.toJson($scope.organizationsStorage);
        alert("Your data successfully updated!");
    };

    $scope.removeOrganization = () => {
        for (key in $scope.organizationsStorage) {
            if ($scope.organizationsStorage[key].id == $scope.selectedItem.id) {
                $scope.organizationsStorage.splice(key, 1);
            }
        }

        $scope.selectedItem = $scope.organizationsStorage[0];
        window.localStorage["organizations"] = angular.toJson($scope.organizationsStorage);
    };

    $scope.addNewOrganization = () => {
        if ($scope.newOrganization) {
            for (key in $scope.organizationsStorage) {
                if ($scope.organizationsStorage[key].id == $scope.newOrganization.id) {
                    alert("current ID is already in use");
                    return;
                }
            }

            $scope.organizationsStorage.push($scope.newOrganization);
            window.localStorage["organizations"] =  angular.toJson($scope.organizationsStorage);
        }
    };

    $scope.getSelectedItem = (item) => {
        item ? $scope.selectedItem = item : $scope.selectedItem = $scope.organizationsStorage[0];
    };
})