# Rogmax test-task

## Build Setup

``` bash
# install dependencies

npm install
bower install

# run build tasks with hot reload

gulp

```

You should open page on `localhost:8000`