var gulp         = require('gulp'),
    path         = require('path'),
    watch        = require('gulp-watch'),
    postcss      = require('gulp-postcss'),
    webserver    = require('gulp-webserver'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer');


gulp.task('lint-css', function lintCssTask() {
  const gulpStylelint = require('gulp-stylelint');

  return gulp
    .src('src/css/*.css')
    .pipe(gulpStylelint({
      reporters: [
        {formatter: 'string', console: true}
      ]
    }));
});

gulp.task('css', function () {
    return gulp.src('src/css/*.css')
        .pipe( postcss([ require('postcss-nested'), require('postcss-simple-vars') ]) )
        .pipe( gulp.dest('app/css') );
});

gulp.task('default', ['lint-css', 'css'] , () => {
    gulp.src('app/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({
            browsers: [
                'last 2 versions',
                'ie > 10'
            ]
        }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/css'));

    gulp.src('app')
        .pipe(webserver({
            fallback: 'index.html',
            livereload: true,
            directoryListing: false,
            open: false
    }));

    return watch('src/**/*.css', ['lint-css', 'css'], function () {
        const gulpStylelint = require('gulp-stylelint');

        gulp.src('src/css/*.css')
            .pipe(gulpStylelint({
                reporters: [
                    {formatter: 'string', console: true}
                ]
            }));
        gulp.src('src/css/*.css')
            .pipe( postcss([ require('postcss-nested'), require('postcss-simple-vars') ]) )
            .pipe( gulp.dest('app/css') );
    });
});
